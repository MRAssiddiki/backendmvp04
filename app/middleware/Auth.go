package middleware

import (
	"fmt"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

type GetClaims struct {
	AccountNumber int `json:"account_number"`
	jwt.StandardClaims
}

func Auth(c *gin.Context) {
	tokenString := c.Request.Header.Get("Authorization")
	//tokenString, _ := c.Cookie("value")
	cekclaims := &GetClaims{}
	token, err := jwt.ParseWithClaims(tokenString, cekclaims, func(token *jwt.Token) (interface{}, error) {
		if jwt.GetSigningMethod("HS256") != token.Method {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return []byte("secret"), nil
	})

	if token != nil && err == nil {
		fmt.Println("Token verified")
		fmt.Println(cekclaims)

		c.Set("account_number", cekclaims.AccountNumber)
	} else {
		result := gin.H{
			"message": "SILAHKAN LOGIN TERLEBIH DAHULU !",
		}

		c.JSON(http.StatusUnauthorized, result)
		c.Abort()
	}
}
