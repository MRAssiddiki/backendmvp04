package model

import (
	"begorm/app/utils"

	"github.com/dgrijalva/jwt-go"

	"github.com/pkg/errors"

	"gorm.io/gorm"
)

var DB *gorm.DB

type InDB struct {
	DB *gorm.DB
}

type Account struct {
	//DB            *gorm.DB
	ID       int    `gorm:"primary_key" json:"-"`
	Name     string `json:"name"`
	Email    string `json:"email"`
	Password string `json:"password,omitempty"`
	Nohp     string `json:"nohp"`
}

type Auth struct {
	//DB       *gorm.DB
	Email    string `json:"email"`
	Password string `json:"password"`
}

type Transaction struct {
	//DB                     *gorm.DB
	ID                     int    `gorm:"primary_key" json:"-"`
	TransactionType        int    `json:"transaction_type,omitempty"`
	TransactionDescription string `json:"transaction_description"`
	Sender                 int    `json:"sender"`
	Amount                 int    `json:"amount"`
	Recipient              int    `json:"recipient"`
	Timestamp              int64  `json:"timestamp,omitempty"`
}

func (inDB InDB) Login(auth Auth) (bool, error, string) {
	var account Account
	if err := inDB.DB.Where(&Account{Email: auth.Email}).First(&account).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return false, errors.Errorf("account not found"), ""
		}
	}

	err := utils.HashComparator([]byte(account.Password), []byte(auth.Password))

	if err != nil {
		return false, errors.Errorf("incorrect password"), ""
	}

	sign := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"email": account.Email,
		"name":  account.Name,
	})

	token, err := sign.SignedString([]byte("secret"))
	if err != nil {
		return false, err, ""
	}

	return true, nil, token
}

func (inDB InDB) InsertNewAccount(account Account) (bool, error) {

	result := inDB.DB.Where(&Account{Email: account.Email}).First(&account)
	if result.RowsAffected > 0 {
		return false, errors.Errorf("Akun Anda Sudah Terdaftar")
	}

	if err := inDB.DB.Create(&account).Error; err != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}

	return true, nil
}
